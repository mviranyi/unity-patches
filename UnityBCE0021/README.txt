Unity Patch for issue BCE0021
-----------------------------

Simply looks up and removes the UnityEditor reference tag from the specified 
.unityprof files. Filepaths are provided through the config file.
Use Ctrl + C for termination.

For Windows.

Usage: 

  UnityBCE0021.exe [OPTIONS]

Options:

  -d Dry Run, doesn't affect the files.
  -f File check interval, miliseconds between each file.
  -i Overall Interval in miliseconds.
  -l Limit the execution to a certain amount, e.g. 1, default -1 (Infinite).
  -o Display active Options in prompt.

Examples

  UnityBCE0021.exe -o
  
  Shows you the options with all the default values.
  
  UnityBCE0021.exe -l 1 -i 100 -f 100 -o
  
  One shot script which would take 300ms with two files.

Hint

  Open Unity and this executable together via a shortcut (look into Shortcut 
  folder).

Detailed description:

  Deletes '<Reference Include="UnityEditor">..</Reference>' from the files.